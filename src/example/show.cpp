#include <iostream>

class Interger {
public:
  int i;
public:
  Interger(int ii) : i(ii) {}
  ~Interger() {}

  const Interger operator+(const Interger &j) const {
    std::cout << "Interger operator+\n";
    return Interger(i + j.i); 
  }

  Interger& operator+=(const Interger &j) {
    std::cout << "Interger operator+=\n";
    i += j.i;
    return *this;
  }
};

int main() {
  Interger a(1), b(2), c(3);
  c += a + b;

  std::cout << "a is: " << a.i << "\n";
  std::cout << "b is: " << b.i << "\n";
  std::cout << "c is: " << c.i << "\n";
}