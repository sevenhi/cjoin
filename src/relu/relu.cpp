#include "relu.h"

template <typename T>
void relu(T *dst, T *src, int size)
{
  for (int i = 0; i < size; i++)
  {
    dst[i] = src[i] > 0 ? src[i] : 0;
  }
}
