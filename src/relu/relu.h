#ifndef SRC_RELU_RELU_H_
#define SRC_RELU_RELU_H_
template <typename T> void relu(T *dst, T *src, int size);
#endif // SRC_RELU_RELU_H_
